#!/bin/sh

mpr_util_home=$(dirname "$0")/..
build_classes=$mpr_util_home/target/classes
jar=$mpr_util_home/target/mpr-util-1.0.jar

if [ -d "$build_classes" ]; then
    export CLASSPATH=$build_classes:$CLASSPATH
else if [ -f "$jar" ]; then
    export CLASSPATH=$jar:$CLASSPATH
else
    echo "Cannot resolve mpr utilities classpath" >&2
    echo "(looking for $build_classes or $jar)" >&2
    exit 1
fi;fi
