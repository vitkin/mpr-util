#!/bin/sh

mpr_util_home=$(dirname "$0")/..

for f in "$@"; do
    echo "Following $f ..."
    $mpr_util_home/bin/strspl.sh -f -l 72 "$f" out
done